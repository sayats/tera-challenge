package com.satybald.parser

import com.satybald.model.TypeOps.Config
import com.satybald.parser.CsvParser.logger

import scala.util.{Failure, Success, Try}

object CmdParser {
  def parse(args: Array[String]): Option[Config] = args match {
    case Array(distanceMeter, timeMin, filePath) =>
      Try {
        Some(Config(distanceMeter.toInt, timeMin.toInt, filePath))
      } match {
        case Success(value) => value
        case Failure(error: Exception) =>
          logger.error("Error parsing CMD args {}. Using default one",
                       error.getMessage)
          None
      }
    case _ @missformedRow =>
      logger.warn("CMD has not enought arguments {}. Using default",
                  missformedRow)
      None
  }
}

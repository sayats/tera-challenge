package com.satybald.parser

import java.time.format.DateTimeFormatter
import java.time.{ZoneId, ZonedDateTime}

import com.satybald.model.TypeOps.{Config, Point, Row}
import com.satybald.util.Logging

import scala.util.{Failure, Success, Try}

object CsvParser extends Logging {
  def parse(line: String): Option[Row] = {
    line.split(",") match {
      case Array(ts, x, y, floor, uid) =>
        Try {
          val localDate = ZonedDateTime.parse(
            ts,
            DateTimeFormatter.ISO_INSTANT.withZone(ZoneId.of("UTC")))
          Some(Row(localDate, Point(x.toDouble, y.toDouble), floor.toInt, uid))
        } match {
          case Success(value) => value
          case Failure(error: Exception) =>
            logger.error("Error parsing CSV {}", error.getMessage)
            None
        }
      case _ @missformedRow =>
        logger.error("CSV does not have 5 columns. Row: {}", missformedRow)
        None
    }
  }
  def readLines(config: Config): List[Row] = {
    val source = scala.io.Source.fromFile(config.filePath)
    val lines = source.getLines().drop(1).flatMap(parse(_))
    lines.toList
  }
}

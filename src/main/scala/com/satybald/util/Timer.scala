package com.satybald.util

object Timer extends Logging {
  def timed[T](name: String)(block: => T): T = {
    val start = System.nanoTime()
    val result = block
    val end = System.nanoTime()
    val diff = (end - start) / 1000 / 1000
    logger.info(s"$name elapsed: $diff ms")
    result
  }
}

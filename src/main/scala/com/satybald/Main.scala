package com.satybald

import com.satybald.algo.Compute
import com.satybald.model.Graph
import com.satybald.parser.{CmdParser, CsvParser}
import com.satybald.model.TypeOps.Config
import com.satybald.util.Timer

object Main extends App {
  Timer.timed("total") {
    val defaultConfig =
      Config(distanceMeters = 1,
             intervalMinute = 1,
             filePath = getClass.getResource("/small.csv").getPath)
    val config = CmdParser.parse(args).getOrElse(defaultConfig)
    val dataByFloor = CsvParser.readLines(config).groupBy(_.floor)
    val result = dataByFloor
      .map {
        case (_, floorPoints) =>
          Compute.findPeopleThatIntersect(floorPoints, config)
      }
      .foldLeft(Graph.empty)(_ merge _)
    println(result)
  }
}

package com.satybald.algo

import com.satybald.model.Graph
import com.satybald.model.TypeOps._
import com.satybald.util.Timer.timed

object Compute {
  def findPeopleThatIntersect(points: Seq[Row], config: Config): Graph = {
    val graph = Graph.empty
    val xSorted = timed("xSorted") {
      points.sorted(SortByXRowOrdering)
    }
    val ySorted = timed("ySorted") {
      points.sorted(SortByYRowOrdering)
    }

    timed("process xSorted Points") {
      process(xSorted, graph, config)
    }
    timed("process ySorted Points") {
      process(ySorted, graph, config)
    }
    graph
  }

  private def process(timeLines: Seq[Row], graph: Graph, config: Config) = {
    if (timeLines.size > 1) {
      timeLines.zip(timeLines.tail).foreach {
        case (left, right) =>
          if (left.hasMeetByDistance(right, config.distanceMeters)
              && left.isNotSameUID(right)
              && left.hasMeetByTime(right, config.intervalMinute)) {
            graph.addNodes(left, right)
          }
      }
    }
  }
}

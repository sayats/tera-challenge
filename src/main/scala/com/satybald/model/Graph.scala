package com.satybald.model

import com.satybald.model.TypeOps.{Row, UID}

import scala.collection.mutable

/**
  * Represents a bi directional graph without weights. It stores information
  */
class Graph {
  private val nodes = mutable.Map[UID, Set[Row]]().withDefaultValue(Set())

  private def addNode(node1: Row, node2: Row): Unit = {
    val newNodes = nodes(node1.uid) + node2
    nodes += (node1.uid -> newNodes)
  }
  def addNodes(node1: Row, node2: Row): Unit = {
    addNode(node1, node2)
    addNode(node2, node1)
  }

  def merge(that: Graph): Graph = {
    for ((key, value) <- that.nodes) {
      if (nodes.contains(key)) {
        nodes += key -> (nodes(key) ++ value)
      } else {
        nodes += key -> value
      }
    }
    this
  }

  def getNodes(nodeId: UID): Set[Row] = nodes(nodeId)

  def getSize: Int = nodes.size

  override def toString: String = {
    nodes.toSeq
      .map {
        case (id, rows) =>
          id + s" has been meet ${rows.size} times\n\t" + rows.mkString("\n\t")
      }
      .mkString("\n")
  }
}
object Graph {
  def empty: Graph = new Graph()
}

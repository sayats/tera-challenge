package com.satybald.model

import java.time.{Duration, ZonedDateTime}

object TypeOps {
  type UID = String

  case class Row(timestamp: ZonedDateTime,
                 point: Point,
                 floor: Int,
                 uid: String) {
    def hasMeetByTime(that: Row, interval: Int): Boolean = {
      if (Duration
            .between(timestamp, that.timestamp)
            .abs()
            .toMinutes <= interval) true
      else false
    }

    def isNotSameUID(that: Row): Boolean = {
      uid != that.uid
    }

    def hasMeetByDistance(that: Row, radius: Int): Boolean = {
      val distance = point.l1Norm(that.point)
      if (distance <= radius) true else false
    }

    override def toString: String = {
      s"\t| $timestamp | \t$uid | \t$floor | \t$point\t"
    }

  }

  case class Point(x: Double, y: Double) {
    def l1Norm(that: Point): Int = {
      (math.abs(x - that.x) + math.abs(y - that.y)).toInt
    }

    override def toString: String = s"($x, $y)"
  }

  case class Config(distanceMeters: Int, intervalMinute: Int, filePath: String)

  object TimeZoneOrdering extends Ordering[ZonedDateTime] {
    override def compare(x: ZonedDateTime, y: ZonedDateTime): Int =
      x.compareTo(y)
  }
  object SortByXRowOrdering extends Ordering[Row] {
    override def compare(lhs: Row, rhs: Row): Int =
      lhs.point.x compare (rhs.point.x)
  }
  object SortByYRowOrdering extends Ordering[Row] {
    override def compare(lhs: Row, rhs: Row): Int =
      lhs.point.y compare (rhs.point.y)
  }
}

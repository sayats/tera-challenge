package com.satybald.model

import java.time.ZonedDateTime

import com.satybald.model.TypeOps.{Point, Row}
import org.specs2.mutable.Specification

class GraphTest extends Specification {

  "GraphTest" should {
    "merge two empty graphs" in {
      val result = Graph.empty merge Graph.empty
      result.getSize mustEqual 0
    }

    "merge graph with the same uid" in {
      val graph = Graph.empty
      val left = Row(ZonedDateTime.now(), Point(1, 1), 0, "1")
      val right = Row(ZonedDateTime.now(), Point(1, 0), 0, "1")
      graph.addNodes(left, right)
      graph.getNodes("1").size mustEqual 2
    }

    "merge graph with different uid" in {
      val graph = Graph.empty
      val left = Row(ZonedDateTime.now(), Point(1, 1), 0, "1")
      val right = Row(ZonedDateTime.now(), Point(1, 0), 0, "2")
      graph.addNodes(left, right)
      graph.getNodes("1").size mustEqual 1
      graph.getNodes("2").size mustEqual 1
    }
  }
}

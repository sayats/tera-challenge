## Teralytics challenge

### Design:
The program is a plain scala app, with minimum library and frameworks dependencies.

In order find points that might meet, following is a criterion for a meeting:

* people must be in the same floor,
* be within 1 meter of Manhattan distance,
* 1 minute of time intersection.
T
hese parameters are configured in Config class. They can be passed via arguments to Main class or hard coded in the app.

To keep it simple, I used just plain scala primitives to solving this tasks.

### Performance of the algorithm, i.e., space and time complexity
Run time complexity is bound by sorting: O(N * Log N). Memory: O(N).

I did couple benchmarks and found that most of the time CPU is taken by boxing to doubles, which is done in SortByXRowOrdering comparator classes. Using hash approach can significantly speed up implementation.

### What changes are necessary to support either

(i) a large batch of queries,
(ii)repeated queries à la web service
Depending on the size of the queries and size of the points, we can use different strategies.
If the size of data points fits in RAM, it's better to build an in-memory index out of that for geo queries. One of the implementation could be a [K-d Tree](https://en.wikipedia.org/wiki/K-d_tree). If some error is acceptable, another one is to use [Geo hash](https://en.wikipedia.org/wiki/Geohash).

(iii) an infinite stream of input.
Such stream would not able to fit in RAM in a single node instance, for that we need to think
about how to distribute a load within a cluster of nodes. One approach to use existing out box solutions such as Elastic Search(ES), which provides a spatial indexing, and closest point search.
Alternatively, we can implement such solution using a lambda architecture.


### How to Run
To execute a program you can either use IDE or run via sbt. Please don't forget to specify a proper JVM heap size.
```sh
export SBT_OPTS="-Xmx2G -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -Xss2M"
sbt "run distance_meneters time_interval_min full_path_to_csv_file"
```

### Known limitations:
* number of rows in CSV file should less than Int.MaxValue i.e. ~2 billion rows
